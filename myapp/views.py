from django.shortcuts import render
from myapp.models import News, Cube


def index(request):
    field_names = [f.name for f in News._meta.get_fields()]
    data_news = [[getattr(ins, name) for name in field_names]
            for ins in News.objects.prefetch_related().all()]

    return render(request, "index.html", {"data_news": data_news})


def matches(request):
    return render(request, "matches.html", {})

