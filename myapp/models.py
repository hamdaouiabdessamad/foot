from django.db import models


class News(models.Model):
    id = models.AutoField(primary_key=True)
    image = models.TextField()


class Cube(models.Model):
    id = models.AutoField(primary_key=True)
    Nom = models.CharField(max_length=30)
    logo = models.TextField()
    points = models.IntegerField(max_length=3)


class Match(models.Model):
    id = models.AutoField(primary_key=True)
    id1 = models.IntegerField(max_length=2)
    id2 = models.IntegerField(max_length=2)
    result = models.CharField(max_length=6)
    date = models.CharField(max_length=100)
    hour = models.CharField(max_length=100)

