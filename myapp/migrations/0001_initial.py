# Generated by Django 3.2.4 on 2021-07-11 12:01

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Cube',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('Nom', models.CharField(max_length=30)),
                ('logo', models.TextField()),
            ],
        ),
        migrations.CreateModel(
            name='News',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('image', models.TextField()),
            ],
        ),
    ]
